import 'weui'
import weui from 'weui.js'
import Vue from 'vue'
import Vuex from 'vuex'
import Router from 'vue-router'
import VueCookies from 'vue-cookies'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import EleForm from "vue-ele-form";
import 'es6-promise/auto'

Vue.use(Vuex);
Vue.use(Router);
Vue.use(VueCookies);
Vue.use(ElementUI);
Vue.use(EleForm);
Vue.prototype.$weui = weui;

import form_design from '@/components/Form_design'
import form_display from '@/components/Form_display'
import form_index from '@/components/Form_index'
import form_redirect from '@/components/Form_redirect'
import form_ds from '@/components/Form_ds'

export default new Router({
  routes: [
    {
      path: '/form-ds',
      name: 'form_ds',
      component: form_ds
    },
    {
      path: '/form-index',
      name: 'form_index',
      component: form_index
    },
    {
      path: '/form-design/',
      name: 'form_design',
      component: form_design
    },
    {
      /** 此id为表单创建数据 存在mongodb中的 objectId */
      path: '/form-display/:id',
      name: 'form_display',
      component: form_display
    },
    {
      path: '/form-redirect',
      name: 'form_redirect',
      component: form_redirect
    }
  ]
})
